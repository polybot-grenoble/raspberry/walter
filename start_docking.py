from time import sleep
from typing import Dict, Generator, Type, List, Tuple
from threading import Thread
from time import sleep
from math import pi, cos, sin, acos, sqrt

from numbers import Number

from ihm import stm, client_mqtt  # , lidar
from ihm.strat import Strategie

from ihm.stm import StmManager, coordinate_system
from ihm.coordinate_system import Position
from ihm.client_mqtt import Superviseur

from ihm.strat import TeamColor

from ihm.posManager import PosManager


mm = float
rad = float
deg = int
Pos2d = Tuple[mm, mm, rad] 
Vec2d = Tuple[mm, mm] 

OPEN: deg = 90
CLOSE: deg = 0
POSX_depart: mm = 200.0
POSY_depart: mm = 200.0
RAYON_GATEAU: mm = 60
RAYON_RANGE_GATEAU: mm = 100
MARGE: mm = 15

RANGEMENTS = [ # Comme ça on a juste à changer l'ordre si c'est pas bon
    (CLOSE, CLOSE, OPEN, CLOSE),
    (CLOSE, CLOSE, CLOSE, OPEN),
    (OPEN, CLOSE, CLOSE, CLOSE),
    (CLOSE, OPEN, CLOSE, CLOSE),
]
RANGEMENTS_CLOSEALL = (CLOSE, CLOSE, CLOSE, CLOSE)

# Calculateur de trajectoire (attention aux murs !!! il ne les prend pas en compte)
class Docking():

    def __init__(self, rw: mm, rg: mm, g: mm, marge: mm) -> None:
        """
        rw      : rayon walter
        rg      : rayon gâteau
        g       : distance entre le centre de walter et le centre d'un range-gâteau
        marge   : distance à laisser entre le robot et le gâteau à l'arrivée
        """
        
        self.marge = marge

        self.rangement = 0
        
        self._gateau: Pos2d = (0, 0, 0)
        self._position: Pos2d = (0, 0, 0)

        self._rw = rw
        self._rg = rg
        self._g = g


    @property 
    def rangement (self) -> int:
        return self._rangement
    
    @rangement.setter
    def rangement (self, r: int) -> None:
        
        if (r < 0) or (r > 3):
            raise Exception(f"[Docking] Le port d'accueil {r} n'existe pas.")
        
        self._rangement = r
        self._beta = pi * ( 0.5 * r - 0.75 )
        
    @property 
    def gateau (self) -> Position:
        x, y, theta = self._gateau
        return Position(x, y, theta)
    
    @gateau.setter
    def gateau (self, g: Position): # Position dans le repère absolu plateau
        self._gateau = (g.x, g.y, g.teta) # Changer ici si le type Pos2d change !

    @property 
    def position (self) -> Position:
        x, y, theta = self._position
        return Position(x, y, theta)
    
    @gateau.setter
    def position (self, pos: Position): # Position dans le repère absolu plateau
        self._position = (pos.x, pos.y, pos.teta) # Changer ici si le type Pos2d change !

    def eps (self, dx, dy):
        
        if (dy == 0 and dx < 0):
            return pi
        
        d = sqrt((dx ** 2) + (dy ** 2))
        
        return (1 - 2 * (dy < 0)) * acos(dx / d)

    def dst_angle (self) -> rad: # Calcule l'angle à l'arrivée en repère absolu
        xw, yw, gamma = self._position
        xg, yg, _  = self._gateau
        
        dx = xg - xw
        dy = yg - yw

        # On ne soustrait plus gamma car on ne veut pas la différence entre les angles mais l'angle final
        return self.eps(dx, dy) - self._beta # - gamma 

    def destination (self) -> Pos2d:

        xw, yw, _ = self._position
        xg, yg, _  = self._gateau
        
        dx = xg - xw
        dy = yg - yw
        d = sqrt((dx ** 2) + (dy ** 2))

        d2 = d - self._rw - self._rg - self.marge
        ratio = d2 / d

        x = xw + ratio * dx
        y = yw + ratio * dy
        angle = self.dst_angle()

        return (x, y, angle)   

    def _trajectoire_attrape (self) -> Vec2d:
        dn = self._rg + self.marge + self._rw - self._g

        angle = self.dst_angle() + self._beta
        
        dx = dn * cos(angle)
        dy = dn * sin(angle)
        return (dx, dy)
    
    def _trajectoire_depose (self) -> Vec2d:
        x, y = self._trajectoire_attrape()
        return (-x, -y)
        
    def _chemin_attrape (self) -> List[Position]:
        chemin: List[Position] = []
        
        dst = self.destination()

        x, y, gamma = dst
        dx, dy = self._trajectoire_attrape()

        chemin.append(Position(x, y, gamma))
        chemin.append(Position(x + dx, y + dy, gamma)) # Changer ici si le type Pos2d change !

        return chemin
        
    def _chemin_depose (self) -> List[Position]:
        chemin: List[Position] = []
        
        dst = self.destination()

        x, y, gamma = dst
        dx, dy = self._trajectoire_depose()

        chemin.append(Position(x - dx, y - dy, gamma)) # Changer ici si le type Pos2d change !
        chemin.append(Position(x, y, gamma))

        return chemin
    
    def attrape (self, gateau: Position, robot: Position, rangement: int) -> List[Position]:
        self.gateau = gateau
        self.position = robot
        self.rangement = rangement
        
        return self._chemin_attrape()

    def attrape_instruction (self, pos: List[Position], rangement: int) -> List[Tuple]:
        out = [
            ("pos", pos[0]),
            ("grab", RANGEMENTS[rangement]),
            ("pos", pos[1]),
            ("grab", RANGEMENTS_CLOSEALL),
        ]

        return out

    
    def depose (self, cible: Position, robot: Position, rangement: int) -> List[Position]:
        self.gateau = cible
        self.position = robot
        self.rangement = rangement
        
        return self._chemin_depose()

    def deopse_instruction (self, pos: List[Position], rangement: int) -> List[Tuple]:
        out = [
            ("pos", pos[0]),
            ("grab", RANGEMENTS[rangement]),
            ("pos", pos[1]),
            ("grab", RANGEMENTS_CLOSEALL),
        ]

        return out
    
