from time import sleep
from typing import Dict, Type, Iterable, Tuple
from threading import Thread
from time import sleep
from math import pi
from abc import ABC, abstractmethod
import logging

from ihm import stm, client_mqtt  # , lidar
from ihm.strat import Strategie, StrategieManager

from ihm.stm import StmManager, coordinate_system
from ihm.coordinate_system import Position
from ihm.client_mqtt import Superviseur

from ihm.strat import TeamColor

from ihm.posManager import PosManager

from start_docking import Docking, RAYON_GATEAU, RAYON_RANGE_GATEAU, MARGE

OPEN = 90
CLOSE = 0
POSX_depart = 200.0
POSY_depart = 200.0


class StrategieWithSuperviseur(Strategie, ABC):
    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """
        Superviseur.superviseur.stream_sync()
        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

    def abort(self):
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()


class TestStrat(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [
            ("grab", (0, 0, 0, 0)),
            ("pos", Position(200+500 , 200, pi/4)),
            ("grab", (0, 90, 0, 0)),
            ("pos", Position(200+700,200,pi/4)),
            ("grab", (0, 0, 0, 0)),
            ("pos", Position(200,200,pi/4)),
            ("wait", 3),


            # ESC and Trapdoor usage
            # ("esc", True),
            # ("wait", 3),
            # ("esc", False),
            # ("wait", 3),

            # ("trap", True),
            # ("wait", 3),
            # ("trap", False),
            # ("wait", 3),

        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class HomologationRobot1(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [

            ("pos", Position(950.0, 940.0, 0)),
            ("grab", (0, 0, 0, 90)),
            ("pos", Position(1077.5, 777.5, 0)),
            ("grab", (0, 0, 0, 0)),
            ("pos", Position(1730.0, 350.0, 0)),
            ("grab", (0, 0, 0, 90)),
            
            ("pos", Position(1605.0, 502.5, 0)),
            ("pos", Position(230.0, 502.5, 0)),



            # ("grab", (180, 180, 180, 180)),
            # ("pos", Position(200  , 1500, 0)),
            # ("pos", Position(1500, 1500, 0)),
            # ("pos", Position(1500, 200  , 0)),
            # ("pos", Position(200  , 200  , 0)),
            # ("grab", (90, 90, 90, 90)),
            # ("wait", 3),


            # ("pos", Position(1500, 1500, 0)),
            # ("pos", Position(200  , 1500, 0)),
            # ("pos", Position(1500, 200  , 0)),
            # ("pos", Position(200  , 200  , 0)),


            # ESC and Trapdoor usage
            # ("esc", True),
            # ("wait", 3),
            # ("esc", False),
            # ("wait", 3),

            # ("trap", True),
            # ("wait", 3),
            # ("trap", False),
            # ("wait", 3),

        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class Robot1Basic(StrategieWithSuperviseur):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        super().__init__(team_color, aruco)

        logging.info("Robot1Basic initialising")

        self._team_color = team_color

        self._updated = False
        self.commands = [

            ("pos", Position(310.0, 225.0, 0)),
            ("grab", (0, 0, 0, 90)),
            ("pos", Position(310.0, 225.0, pi/4)),
            ("pos", Position(480.0, 217.5, pi/4)),
            ("grab", (0, 0, 0, 0)),
            # 1er gato choppé

            ("pos", Position(480.0, 217.5, 3*pi/4)),
            ("grab", (0, 0, 90, 0)),
            ("pos", Position(675.0, 217.5, 3*pi/4)),
            ("grab", (0, 0, 0, 0)),
            # 2e gato choppé




            ("pos", Position(860.0, 725.0, 3*pi/4)),
            ("pos", Position(860.0, 725.0, -3*pi/4)),
            ("grab", (0, 90, 0, 0)),
            ("pos", Position(1037.5, 727.5, -3*pi/4)),
            ("grab", (0, 0, 0, 0)),
            #3e gato choppé

            ("pos", Position(187.5, 190.0, -3*pi/4)),
            ("grab", (0, 0, 0, 90)),
            ("pos", Position(332.5, 190.0, -3*pi/4)),
            ("grab", (0, 0, 0, 0)),
            ("pos", Position(332.5, 190.0, -pi/4)),
            ("grab", (0, 0, 90, 0)),
            ("pos", Position(517.5, 190.0, -pi/4)),
            ("grab", (0, 0, 0, 0)),
            ("pos", Position(517.5, 190.0, pi/4)),
            ("grab", (0, 90, 0, 0)),
            ("pos", Position(620.0, 190.0, pi/4)),
            ("grab", (0, 0, 0, 0)),
            ("pos", Position(620.0, 190.0, pi/4)),


            # ("grab", (180, 180, 180, 180)),
            # ("pos", Position(200  , 1500, 0)),
            # ("pos", Position(1500, 1500, 0)),
            # ("pos", Position(1500, 200  , 0)),
            # ("pos", Position(200  , 200  , 0)),
            # ("grab", (90, 90, 90, 90)),
            # ("wait", 3),


            # ("pos", Position(1500, 1500, 0)),
            # ("pos", Position(200  , 1500, 0)),
            # ("pos", Position(1500, 200  , 0)),
            # ("pos", Position(200  , 200  , 0)),


            # ESC and Trapdoor usage
            # ("esc", True),
            # ("wait", 3),
            # ("esc", False),
            # ("wait", 3),

            # ("trap", True),
            # ("wait", 3),
            # ("trap", False),
            # ("wait", 3),

        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    blue_position: Position = self.current_command[1]
                    correct_position: Position = blue_position if self._team_color == TeamColor.BLEU else Position(blue_position.x, 2000 - blue_position.y, -blue_position.teta)
                    PosManager.pos_manager.go_to(correct_position)

                case "grab":
                    blue_grab_list: Iterable[int] = self.current_command[1]
                    correct_grab_list: Iterable[int] = blue_grab_list if self._team_color == TeamColor.BLEU else blue_grab_list[::-1]
                    StmManager.stm_manager.set_grippers(correct_grab_list)
                    sleep(0.2)

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))

            self._updated = True

        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        super().abort()

    def score(self) -> int:
        pass


class ListedStrat(Strategie, ABC):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        self._main_loop_thread = None

        self._team_color = team_color
        self._aruco = aruco

        self._updated = False
        self.commands = self._get_commands()

    @abstractmethod
    def _get_commands(self) -> Iterable[Tuple]:
        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command):
                case ("pos", position):
                    PosManager.pos_manager.go_to(position)

                case ("grab", angle_iterable):
                    StmManager.stm_manager.set_grippers(angle_iterable)
                    sleep(0.2)

                case ("esc", on_off):
                    StmManager.stm_manager.set_ESC(on_off)

                case ("trap", open_close):
                    StmManager.stm_manager.set_Trapdoor(open_close)

                case ("wait", delai_seconds):
                    sleep(delai_seconds)

                case ("deguis",):
                    stmMessage = StmManager.stm_manager.set_Disguise()
                    StmManager.stm_manager.get_response(stmMessage, 100)

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case _:
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        logging.debug("ListedStrat aborting")
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    @abstractmethod
    def score(self) -> int:
        pass


class Demonstrations(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [
            ("pos", Position(310.0, 225.0, 0)),
            ("grab", (CLOSE, CLOSE, CLOSE, OPEN)),
            ("pos", Position(310.0, 225.0, pi/4)),
            ("pos", Position(480.0, 217.5, pi/4)),
            ("grab", (CLOSE, CLOSE, CLOSE, CLOSE)),
            # 1er gato choppé

            ("pos", Position(480.0, 217.5, 3*pi/4)),
            ("grab", (CLOSE, CLOSE, OPEN, CLOSE)),
            ("pos", Position(675.0, 217.5, 3*pi/4)),
            ("grab", (CLOSE, CLOSE, CLOSE, CLOSE)),
            # 2e gato choppé

            ("pos", Position(860.0, 725.0, 3*pi/4)),
            ("pos", Position(860.0, 730.0, -3*pi/4)),
            ("grab", (CLOSE, OPEN, CLOSE, CLOSE)),
            ("pos", Position(1150.5, 730.5, -3*pi/4)),
            ("grab", (CLOSE, CLOSE, CLOSE, CLOSE)),
            #3e gato choppé
            
            ("pos", Position(1875.0, 500.0, -3*pi/4)),
            ("pos", Position(1875.0, 500.0, 0)),

            ("pos", Position(1875.0, 200.0, 0)),

            #On depose les trois gateaux dans l'assiette bleu la plus proche
            ("grab", (CLOSE, CLOSE, OPEN, OPEN)),
            ("pos", Position(1875.0, 380.0, 0)),
            ("grab", (CLOSE, OPEN, CLOSE, CLOSE)),
            ("pos", Position(1875.0, 380.0, -3*pi/4)),
            ("pos", Position(1875.0, 380.0, 0)),
            ("grab", (CLOSE, CLOSE, CLOSE, CLOSE)),

            #Retour au bercail
            ("pos", Position(POSX_depart, POSY_depart, 0)),
            ("wait", 3),
        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class EntreeSortieVert(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [
            ("pos", Position(POSX_depart + 100 , POSY_depart, 0)),
            ("pos", Position(POSX_depart , POSY_depart, 0)),
        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class StratBleue(ListedStrat):
    # Position initiale : centree sur le plateau (x=225, y=225), angle de pi/4 pour chopper direct le gateau
    def _get_commands(self):
        return [
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(510, 225, pi/4)), # Chopper le glacage
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince glacage
            ("pos", Position(510, 225, 3*pi/4)), # Rotation pour chopper la creme
            ("pos", Position(720, 225, 3*pi/4)), # Chopper la creme
            ("grab", (OPEN, OPEN, CLOSE, CLOSE)), # Fermeture pince creme
            ("pos", Position(1010, 1775, 3*pi/4)), # Gateaux dans la zone
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1125, 1775, pi)), # Walter remis d'aplomb
            ("pos", Position(1125, 1530, pi)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(1125, 1530, pi+pi/4)),  # Rotation pour chopper la genoise
            ("grab", (OPEN, CLOSE, OPEN, OPEN)), # Fermeture pince genoise
            ("pos", Position(1125, 1300, pi+pi/4)), # Chopper la genoise
            ("pos", Position(1125, 1300, pi+3*pi/4)), # Demi-tour a la zone
            ("pos", Position(1125, 1775, pi+3*pi/4)), # Depos du gateau
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1125, 1530, pi+3*pi/4)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(375, 375, pi+3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("pos", Position(375, 375, pi+3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("deguis",),
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, Position(225, 225, pi/4))

    def score(self) -> int:
        return None


class StratBleueFromTheBack(ListedStrat):
    # Position initiale : centree sur le plateau (x=225, y=225), angle de pi/4 pour chopper direct le gateau
    def _get_commands(self):
        return [
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(2490, 1775, -3*pi/4)), # Chopper le glacage
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince glacage
            ("pos", Position(2490, 1775, -pi/4)), # Rotation pour chopper la creme
            ("pos", Position(2280, 1775, -pi/4)), # Chopper la creme
            ("grab", (OPEN, OPEN, CLOSE, CLOSE)), # Fermeture pince creme
            ("pos", Position(2010, 225, -pi/4)), # Gateaux dans la zone
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1875, 225, 0)), # Walter remis d'aplomb
            ("pos", Position(1875, 470, 0)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(1875, 470, pi/4)),  # Rotation pour chopper la genoise
            ("grab", (OPEN, CLOSE, OPEN, OPEN)), # Fermeture pince genoise
            ("pos", Position(1875, 700, pi/4)), # Chopper la genoise
            ("pos", Position(1875, 700, 3*pi/4)), # Demi-tour a la zone
            ("pos", Position(1875, 225, 3*pi/4)), # Depos du gateau
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1875, 470, 3*pi/4)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(375, 375, 3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("pos", Position(375, 375, 3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("deguis",),
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, Position(225, 225, pi/4))

    def score(self) -> int:
        return None


class StratVert(ListedStrat):
    # Position initiale : centree sur le plateau (x=225, y=1775), angle de pi/4 pour chopper direct le gateau
    def _get_commands(self):
        return [
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(510, 1775, pi/4)), # Chopper le glacage
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince glacage
            ("pos", Position(510, 1775, -pi/4)), # Rotation pour chopper la creme
            ("pos", Position(720, 1775, -pi/4)), # Chopper la creme
            ("grab", (CLOSE, OPEN, OPEN, CLOSE)), # Fermeture pince creme
            ("pos", Position(1010, 225, -pi/4)), # Gateaux dans la zone
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1125, 225, -pi)), # Walter remis d'aplomb
            ("pos", Position(1125, 470, -pi)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(1125, 470, -pi-pi/4)),  # Rotation pour chopper la genoise
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince genoise
            ("pos", Position(1125, 700, -pi-pi/4)), # Chopper la genoise
            ("pos", Position(1125, 700, -pi/4)), # Demi-tour a la zone
            ("pos", Position(1125, 225, -pi/4)), # Depos du gateau
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1125, 470, -pi/4)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(375, 1610, -pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("pos", Position(375, 1610, -pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("deguis",),
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, Position(225, 1775, pi/4))

    def score(self) -> int:
        return None


class StratVertFromTheBack(ListedStrat):
    # Position initiale : centree sur le plateau (x=225, y=1775), angle de pi/4 pour chopper direct le gateau
    def _get_commands(self):
        return [
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(2490, 225, -3*pi/4)), # Chopper le glacage
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince glacage
            ("pos", Position(2490, 225, 3*pi/4)), # Rotation pour chopper la creme
            ("pos", Position(2280, 225, 3*pi/4)), # Chopper la creme
            ("grab", (CLOSE, OPEN, OPEN, CLOSE)), # Fermeture pince creme
            ("pos", Position(1990, 1775, 3*pi/4)), # Gateaux dans la zone
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1875, 1775, 0)), # Walter remis d'aplomb
            ("pos", Position(1875, 1530, 0)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(1875, 1530, -pi/4)),  # Rotation pour chopper la genoise
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince genoise
            ("pos", Position(1875, 1300, -pi/4)), # Chopper la genoise
            ("pos", Position(1875, 1300, 3*pi/4)), # Demi-tour a la zone
            ("pos", Position(1875, 1775, 3*pi/4)), # Depos du gateau
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(1875, 1530, 3*pi/4)), # Eviter d'embarque les gateaux qu'on vient de deposer
            ("pos", Position(2625, 390, 3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("pos", Position(2625, 390, 3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("deguis",),
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, Position(225, 1775, pi/4))

    def score(self) -> int:
        return None


class StratDocking (ListedStrat):
    def _pos_init (self):
        # Position initiale : centree sur le plateau (x=225, y=1775), angle de pi/4 pour chopper direct le gateau
        return Position(225, 1775, pi/4)

    def _get_commands(self):
        # On initialise le calculateur de trajectoires
        docker = Docking(PosManager.pos_manager.robot_radius, RAYON_GATEAU, RAYON_RANGE_GATEAU, MARGE)

        # On définit nos gâteaux 
        gateau0 = Position(0, 0, 0)     
        gateau1 = Position(0, 0, 0)

        # On définit les endroits où on les dépose
        cible0 = Position(0, 0, 0)
        cible1 = Position(0, 0, 0)

        # On récupère la position initaiale
        depart_robot = self._pos_init()

        # On calcule l'ensemble des trajectoires
        liste_attrape = docker.attrape(gateau0, depart_robot, 0)
            # On prend la deuxième position dans le tableau qui deviendra celle du robot
        liste_attrape1 = docker.attrape(gateau1, liste_attrape[1], 1)

        liste_depose = docker.depose(cible0, liste_attrape1[1], 0)

        liste_depose1 = docker.depose(cible1, liste_depose[1], 1)

        return [
            # On convertit nos positions en instructions
            *docker.attrape_instruction(liste_attrape, 0),
            *docker.attrape_instruction(liste_attrape1, 1),
            *docker.deopse_instruction(liste_depose, 0),
            *docker.deopse_instruction(liste_depose1, 1),
            ("deguis", 12),
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, self._pos_init())

    def score(self) -> int:
        return None



class FinalBlueStategy(ListedStrat):
    # Position initiale : centree sur l'assiette bleue du fond (x=2775, y=1775), angle de pi/4 pour chopper direct le glacage
    def _get_commands(self):
        return [
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(540, 225, pi/4)), # Chopper le glacage
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince glacage
            ("pos", Position(540, 225, -pi/4)), # Rotation pour chopper la creme            
            ("pos", Position(775, 225, -pi/4)), # Chopper la creme            
            ("grab", (CLOSE, OPEN, OPEN, CLOSE)), # Fermeture pince creme
            ("pos", Position(775, 225, -3*pi/4+3*pi/10)), # Rotation pour chopper la 1ere genoise  
            ("pos", Position(1125, 725, -3*pi/4+3*pi/10)), # Chopper la genoise 
            ("grab", (CLOSE, CLOSE, OPEN, CLOSE)), # Fermeture pince genoise
            ("pos", Position(1125, 725, -5*pi/4)), # Rotation pour chopper la 2eme genoise
            ("pos", Position(1875, 725, -5*pi/4)), # Chopper la genoise 
            ("grab", (CLOSE, CLOSE, CLOSE, CLOSE)), # Fermeture pince genoise
            ("pos", Position(1875, 205, -pi)), # 1ere genoise et creme dans la zone
            ("grab", (OPEN, OPEN, CLOSE, CLOSE)), # Ouverture pinces 1ere genoise et creme
            ("pos", Position(1875, 450, -pi)), # Eviter d'embarque les couches qu'on vient de deposer
            ("pos", Position(1875, 450, pi/4)), # Rotation pour deposer le glacage
            ("pos", Position(1875, 400, pi/4)), # Glacage dans la zone
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Ouverture pinces glacage
            ("pos", Position(1875, 725, pi/4)), # Eviter d'embarque les couches qu'on vient de deposer          
            ("pos", Position(1875, 725, 3*pi/4)), # Rotation pour deposer la 2eme genoise
            ("pos", Position(1125, 725, 3*pi/4)), # Mvt x pour deposer la 2eme genoise
            ("pos", Position(1125, 1675, 3*pi/4)), # Mvt y pour deposer la 2eme genoise
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces glacage
            ("pos", Position(3000, 1875, 3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("deguis",),            
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, Position(225, 225, pi/4))

    def score(self) -> int:
        return None


class FinalGreenStategy(ListedStrat):
    # Position initiale : centree sur l'assiette bleue du fond (x=2775, y=225), angle de pi/4 pour chopper direct le glacage
    def _get_commands(self):
        return [
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces
            ("pos", Position(540, 1775, pi/4)), # Chopper le glacage
            ("grab", (OPEN, OPEN, OPEN, CLOSE)), # Fermeture pince glacage
            ("pos", Position(540, 1775, -pi/4)), # Rotation pour chopper la creme            
            ("pos", Position(775, 1775, -pi/4)), # Chopper la creme            
            ("grab", (CLOSE, OPEN, OPEN, CLOSE)), # Fermeture pince creme
            ("pos", Position(775, 1775, -3*pi/4-3*pi/10)), # Rotation pour chopper la 1ere genoise  
            ("pos", Position(1125, 1275, -3*pi/4-3*pi/10)), # Chopper la genoise 
            ("grab", (CLOSE, CLOSE, OPEN, CLOSE)), # Fermeture pince genoise
            ("pos", Position(1125, 1275, -5*pi/4)), # Rotation pour chopper la 2eme genoise
            ("pos", Position(1875, 1275, -5*pi/4)), # Chopper la genoise 
            ("grab", (CLOSE, CLOSE, CLOSE, CLOSE)), # Fermeture pince genoise
            ("pos", Position(1875, 1695, -pi)), # 1ere genoise et creme dans la zone
            ("grab", (CLOSE, CLOSE, OPEN, OPEN)), # Ouverture pinces 1ere genoise et creme
            ("pos", Position(1875, 1550, -pi)), # Eviter d'embarque les couches qu'on vient de deposer
            ("pos", Position(1875, 1550, pi/4)), # Rotation pour deposer le glacage
            ("pos", Position(1875, 1550, pi/4)), # Glacage dans la zone
            ("grab", (OPEN, CLOSE, OPEN, OPEN)), # Ouverture pinces glacage
            ("pos", Position(1875, 1275, pi/4)), # Eviter d'embarque les couches qu'on vient de deposer          
            ("pos", Position(1875, 1275, 3*pi/4)), # Rotation pour deposer la 2eme genoise
            ("pos", Position(1125, 1275, 3*pi/4)), # Mvt x pour deposer la 2eme genoise
            ("pos", Position(1125, 125, 3*pi/4)), # Mvt y pour deposer la 2eme genoise
            ("pos", Position(1125, 215, 3*pi/4)), # Eviter d'embarque les couches qu'on vient de deposer 
            ("grab", (OPEN, OPEN, OPEN, OPEN)), # Ouverture pinces glacage
            ("pos", Position(3000, 215, 3*pi/4)), # On rentre a la maison ! Les pieds dans le plat...
            ("deguis",),            
            ("wait", 1)
        ]

    def setup(self):
        PosManager.pos_manager.init_cale_walter_go_to(self._team_color, Position(225, 1775, pi/4))

    def score(self) -> int:
        return None




STRAT_DICT: Dict[str, Type[Strategie]] = {
    "Test strat": TestStrat,
    "Homologation Robot 1": HomologationRobot1,
    "Basic Robot 1": Robot1Basic,
    "Demonstration": Demonstrations,
    "EntreeSortieVert [VERT]": EntreeSortieVert,
    "Strat verte avec fermeture": StratVert,
    "Strat bleue avec fermeture": StratBleue,
    "Strat bleue avec fermeture de l'arrière": StratBleueFromTheBack,
    "Strat verte avec fermeture de l'arrière": StratVertFromTheBack,
    "Match 5 - Strategie finale bleue": FinalBlueStategy,
    "Match 5 - Strategie finale verte": FinalGreenStategy,
}